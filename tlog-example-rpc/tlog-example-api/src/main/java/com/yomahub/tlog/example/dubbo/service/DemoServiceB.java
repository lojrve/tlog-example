package com.yomahub.tlog.example.dubbo.service;

public interface DemoServiceB {

	String sayHello(String name);

	String sayMorning(String name);
}
