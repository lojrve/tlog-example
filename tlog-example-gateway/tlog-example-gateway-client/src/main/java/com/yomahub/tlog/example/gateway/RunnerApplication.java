package com.yomahub.tlog.example.gateway;

import com.yomahub.tlog.example.gateway.feign.TLogFeignClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@RestController
public class RunnerApplication {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private TLogFeignClient client;

    public static void main(String[] args) {
        SpringApplication.run(RunnerApplication.class, args);
    }

    @RequestMapping("/hi")
    public String sayHello(@RequestParam String name){
        log.info("invoke method sayHello,name={}",name);
        return client.sayHello(name);
    }
}
